How to install stuff necessary for this to work in Ubuntu:

sudo apt-get install python-pip python-dev build-essential
sudo pip install --upgrade pip
sudo pip install --upgrade virtualenv
sudo pip install --upgrade distribute
sudo pip install --upgrade nose
