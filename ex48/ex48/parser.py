class ParserError(Exception):
	pass

class Sentence(object):
	
	def __init__(self, subject, verb, object):
		# remember we take ('noun', 'princess') tuples and convert them
		self.subject = subject[1]
		self.verb = verb[1]
		self.object = object[1]



def peek(pWordList):
	if pWordList:
		word = pWordList[0]
		return word[0]
	else:
		return None


def match(pWordList, pExpecting):
	if pWordList:
		word = pWordList.pop(0)

		if(word[0] == expecting:
			return word
		else:
			return None
	else:
		return None

def skip(pWordList, pWordType):
	while peek(pWordList == pWordType)
		match(pWordList, pWordType)
	

def parse_verb(pWordList):
	skip(pWordList, 'stop')

	if(peek(pWordList) == 'verb'):
		return match(pWordList, 'verb')
	else:
		raise ParserError("Expected a verb next.")

def parse_object(pWordList):
	skip(pWordList, 'stop')
	next = peek(pWordList)

	if next == 'noun':
		return match(pWordList, 'noun')
	if next == 'direction':
		return match(pWordList, 'direction')
	else:
		raise ParserError("Expected a noun or direction next")

def parse_subject(pWordList, pSubj):
	verb = parse_verb(pWordList)
	obj = parse_object(pWordList)

	return Sentence(pSubj, vert, obj)

def parse_sentence(pWordList):
	skip(pWordList, 'stop')

	start = peek(pWordList)

	if start == 'noun':
		subj = match(pWordList, 'noun')
		return parse_subject(pWordList, subj)
	elif start == 'verb':
		# assume the subject is the player then
		return parse_subject(pWordList, ('noun', 'player'))
	else:
		raise ParserError("Must start with subject, object, or verb, not %s" % start)
	

