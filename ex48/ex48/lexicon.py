
myLexicon = {
				'direction':	('north', 'south', 'east', 'west'),
				'verb':			('go', 'eat', 'kill'),
				'stop':			('the', 'in', 'of'),
				'noun':			('bear', 'princess')
			}


def scan(pString):
	bits = pString.split()

	tRetval = []

	for tWord in bits:
		print "Scanning lexicon for %r" % tWord
		tCounter = 0
		for tKey, tWords in myLexicon.items():
			if(tWord in tWords):
				tRetval.append((tKey, tWord))
				tCounter += 1
		if(tCounter == 0):
			try:
				tNewVal = int(tWord)
				tRetval.append(('number', tNewVal))
			except ValueError:
				tRetval.append(('error', tWord))
		
	print "RETVAL::: %r" % tRetval
	return (tRetval)

scan("north")
