try:
	from setuptools import setup
except ImportError:
	from distutils.cor import setup
config = {
	'description':		"Exercise #47, LearnPythonTheHardWay.org",
	'author':			"Dan Falconer",
	'url':				"URL to get it at.",
	'download_url':		"Where to download it.",
	'author_email':		"dafalconer@nd.gov",
	'version':			"0.1",
	'install_requires':	['nose'],
	'packages':			['ex47'],
	'scripts':			[],
	'name':				"ex47"
}

setup(**config)
