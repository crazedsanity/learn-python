from pprint import pprint
from inspect import getmembers
import random
import itertools

#==================================
class Item(object):
	Id = 0
	Name = "unknown"
	Qty = 0
	
	#---------------------------------------------
	def __init__(self, pId, pName, pQty):
		self.Id = pId
		self.Name = pName
		self.Qty = pQty
	#---------------------------------------------
#==================================



#==================================
class Inventory(object):
	Items = {}
	
	#---------------------------------------------
	def add_item(self, pId, pName, pQty):
		itemObj = Item(pId, pName, pQty)
		try:
			self.Items[pId] = itemObj
		except:
			# TODO: figure out exceptions
			#print "DEBUG: failed to handle adding... ID=(%r) Name=(%r), Qty=(%r)" % (pId, pName, pQty)
			#print "DEBUG: %r" % self.Items
			self.replace_item(pId, pName, pQty)
	#---------------------------------------------
			
	#---------------------------------------------
	def replace_item(self, pId, pName, pQty):
		itemObj = Item(pId, pName, pQty)
		try:
			del self.Items[pId]
			self.Items[pId] = itemObj
		except:
			#print "DEBUG: failed to handle replacement of the object"
			exit(0)
			# TODO: figure out exceptions!
	#---------------------------------------------
#==================================



#==================================
class Character(object):
	Name = "Unknown"
	HP = 100
	MaxHP = 100
	Inventory = Inventory()
	AttackBonus = 4
	MaxDamage = 1
	Defense = 10
	
	
	#---------------------------------------------
	def __init__(self, name):
		self.Name = name
		self.MaxHP = self.HP
	#---------------------------------------------



	#---------------------------------------------
	def set_hp(self, pHP):
		self.HP = pHP
		self.MaxHP = pHP
	#---------------------------------------------



	#---------------------------------------------
	def roll(self, pModifier):
		tRoll = random.randint(1,20)
		##print "DEBUG::: roll got (%r)" % tRoll
		tResult = tRoll + pModifier
		return (tResult, tRoll)
	#---------------------------------------------



	#---------------------------------------------
	def attack(self, pOpponent):
		#pOpponent.HP = pOpponent.HP - 10
		tMyRoll, tDiceVal = self.roll(self.AttackBonus)

		tIsHit = False
		tIsCrit = False
		tDamage = 0

		if(tMyRoll > pOpponent.Defense or tDiceVal == 20):
			tIsHit = True
			tDamage = self.roll_damage()
			if(tDiceVal == 20):
				tIsCrit = True
				tDamage = tDamage * 2 # Damage gets doubled if it's a crit.

			# Apply damage
			tBeforeHP = pOpponent.HP
			pOpponent.HP = pOpponent.HP - tDamage
			#print "\t\tDEBUG::: Damage=(%d) HP Before=(%d) HP After=(%d)" % (tDamage, tBeforeHP, pOpponent.HP) 
			
		#print "DEBUG: Rolled (%d), Hit=(%s), Crit=(%s), damage=(%d)" % (tMyRoll, tIsHit, tIsCrit, tDamage)
			
		return (tIsHit, tDamage, tIsCrit)
	#---------------------------------------------



	#---------------------------------------------
	def roll_damage(self):
		tRetval = random.randint(1, self.MaxDamage)

		return (tRetval)
	#---------------------------------------------
	


	#---------------------------------------------
	def defend(self, pAttackRoll):
		tRetval = True
		if(pAttackRoll > self.Defense):
			#print "DEBUG: HIT(Defense=%d, Attack Roll=%d)" % (self.Defense, pAttackRoll)
			tRetval = False
		return (tRetval)
	#---------------------------------------------



	#---------------------------------------------
	def is_alive(self):
		tRetval = True		
		if(self.HP <= 0):
			tRetval = False
		return (tRetval)
	#---------------------------------------------
#==================================


#==================================
class Battle:
	
	Participants = {}

	Player = None
	Monster = None	## TODO: let this one be a list to support multiple monsters

	#---------------------------------------------
	def add_player(self, pObj):
		self.Player = pObj
	#---------------------------------------------



	#---------------------------------------------
	def add_monster(self, pObj):
		self.Monster = pObj
	#---------------------------------------------



	#---------------------------------------------
	def show_options(self):
		print """
			Your options are:

			[f] Fight
			[r] Run (coward)
			[q] Quit (exits the program)
		"""
		tValidOptions = "frq"

		tInput = raw_input("> ")
		
		if(len(tInput) > 0 and tInput[0].lower() in tValidOptions):
			tAction = tInput[0].lower()
			if(tAction == "f"):
				## player attacks....
				isHit, tDamage, isCrit = self.Player.attack(self.Monster)
				tOutcome = "You attack %s..." % self.Monster.Name
				if(isHit):
					if(isCrit):
						tOutcome += " AN INCREDIBLE SWING!"
					
					tOutcome = "You hit for %d points of damage." % tDamage
	
					if(self.Monster.is_alive()):
						 #print tOutcome
						 pass
					else:
						tOutcome += """
			You've killed %s! WIN!""" % self.Monster.Name
				else:
					tOutcome = "%s nimbly dodges your attack! YOU MISS!!!" % self.Monster.Name

				print tOutcome
			elif(tAction == "r"):
				print "You want to run???? COWARD!!!! %s stabs you in the back. You die." % self.Monster.Name
				print "ERROR: okay, that was a lie... but I don't know how to handle running.  Goodbye."
				exit(1)
			elif(tAction == "q"):
				print "%s, thou art truly a COWARD." % self.Player.Name
				exit(1)
			else:
				print "ERROR: unknown option %r (%r), exiting." % (tInput, tAction)
				exit(1)
		else:
			if(len(tInput) == 0) :
				print "You need to say something, nimrod."
			else:
				print "I don't understand %r" % (tInput)
			self.show_options()
	#---------------------------------------------



	#---------------------------------------------
	def show_status(self):
		print """
			%s -- the hero (YOU):
			HP=(%d/%d) DEF=%d ATK=%d MAXDMG=%d
			
			%s -- the monster:
			HP=(%d/%d) DEF=%d ATK=%d MAXDMG=%d
		""" % (self.Player.Name, self.Player.HP, self.Player.MaxHP, self.Player.Defense, self.Player.AttackBonus, self.Player.MaxDamage, 
				self.Monster.Name, self.Monster.HP, self.Monster.MaxHP, self.Monster.Defense, self.Monster.AttackBonus, self.Monster.MaxDamage)
	#---------------------------------------------



	#---------------------------------------------
	def start(self):
		if(self.Monster.is_alive() and self.Player.is_alive()):
			tList = None
	
			print """
				***** BATTLE *****"""
			
			self.show_status()
	
			tSanityCounter = 100
			tRoundCounter = 0
			while (self.Player.is_alive() and self.Monster.is_alive() and tSanityCounter > 0):
				tSanityCounter -= 1
				tRoundCounter += 1
	
				## do battle
				isHit, tDamage, isCrit = self.Monster.attack(self.Player)
	
				tOutcome = "%s attacks you!" % self.Monster.Name
	
				if(isHit):
					if(isCrit):
						tOutcome += " A TERRIBLE BLOW!"
						# Criticals get double damage
					tOutcome += " You take %d points of damage." % tDamage
				else:
					tOutcome = "%s misses horribly!" % self.Monster.Name
				
				# Tell them what happened....
				print tOutcome
	
				if(self.Player.is_alive()):
					#print """
					#	You're still alive!!!
					#	You currently have %d/%d hit points.
					#""" % (self.Player.HP, self.Player.MaxHP)
					pass
				else:
					print """
						R.I.P. 
						
						Here lies %s.
						Fought a %s and died after %d rounds.
						Very sad.
					""" % (self.Player.Name, self.Monster.Name, tRoundCounter)
					exit(0)
	
				## Show current status of player and monster
				#if(tRoundCounter > 1):
				self.show_status()
				## PLAYER's TURN!!!
				self.show_options()
	
			if(tSanityCounter == 0):
				print """
					YOU... SHALL NOT... PASS!!!!!!!!
	
					(ERROR: too many loops, exiting)
				"""
				exit(1)
		else:
			print "The monster in this room (%s) is dead." % self.Monster.Name
					
	#---------------------------------------------
#==================================



#==================================
class Monster(Character):
	
	#---------------------------------------------
	def __init__(self, name):
		self.Name = name
	#---------------------------------------------
#==================================



#==================================
class DungeonRoom(object):
	
	Doors = ""
	Description = ""
	X = 0
	Y = 0
	Monster = None

	
	#---------------------------------------------
	def __init__(self, pDoors, pDescription, pX, pY):
		self.Doors = pDoors.lower()
		self.Description = pDescription
		self.X = pX
		self.Y = pY
	#---------------------------------------------
	

	#---------------------------------------------
	def is_door_valid(self, pDirection, pCurrentX, pCurrentY):
		pDirection = pDirection.lower()
		pDirection = pDirection[0:1]
		
		##print "DEBUG: MY DIRECTION: %r, DOORS: %r" % (pDirection, self.Doors)
		
		tIsValid = False
		tX = pCurrentX
		tY = pCurrentY
		if(len(pDirection) > 0 and pDirection in self.Doors):
			tIsValid = True
			
			#now figure out where they're going next
			if(pDirection == 'n'):
				tX = tX - 1
			elif(pDirection == 'e'):
				tY = tY + 1
			elif(pDirection == 's'):
				tX = tX + 1
			elif(pDirection == 'w'):
				tY = tY - 1
			else:
				print "DEBUG: This error should never happen.  And %r is an invalid direction." % pDirection
				exit(1)
		else:
			pass


		return (tIsValid, tX, tY)
	#---------------------------------------------
#==================================



#==================================
class Dungeon(object):
	
	thePlayer =  Character("Unknown")
	CurrentX = 0
	CurrentY = 0
	Grid = {}
	
	#---------------------------------------------
	def __init__(self, pPlayer):
		self.thePlayer = pPlayer
	#---------------------------------------------


	
	#---------------------------------------------
	def get_room_from_coords(self, pX, pY):
		theRoom = "%d.%d" % (pX, pY)
		return(theRoom)
	#---------------------------------------------



	#---------------------------------------------
	def add_room(self, pDungeonRoom):
		theRoom = self.get_room_from_coords(pDungeonRoom.X, pDungeonRoom.Y)
		
		if(theRoom in self.Grid):
			print "ERROR::: that room (%r) already exists" % theRoom
			exit(1)
		else:
			self.Grid[theRoom] = pDungeonRoom
	#---------------------------------------------



	#---------------------------------------------
	def add_monster(self, pMonster, pX, pY):
		theRoom = self.get_room_from_coords(pX, pY) 
		
		if(theRoom in self.Grid):
			self.Grid[theRoom].Monster = pMonster
		else:
			print "ERROR: that room (%r) does not exist" % theRoom
	#---------------------------------------------


	
	#---------------------------------------------
	def start(self):
		print """
		Welcome to my text-based dungeon program!
		"""
		self.enter_room(0,0)
	#---------------------------------------------
	
	
	#---------------------------------------------
	def enter_room(self, x,y):
		print "++++" * 10
		#print "DEBUG: entering room: x=%r y=%r" % (x, y)
		theRoom = "%d.%d" % (x,y)
		#print "DEBUG: theRoom=%r" % theRoom
		
		if(theRoom in self.Grid):
			self.CurrentX = x
			self.CurrentY = y
		
			tDescription = self.Grid[theRoom].Description

			print tDescription
			
			if(self.Grid[theRoom].Monster != None):
				tBattle = Battle()
				tBattle.add_player(self.thePlayer)
				tBattle.add_monster(self.Grid[theRoom].Monster)
				tBattle.start()

			if(x == 3 and y == 3):
				## WIN!!!!
				exit(0)
			else:
				print """
		### MOVEMENT OPTIONS ###
		%s
			""" % (self.create_path_options(self.Grid[theRoom].Doors))
			
		
		else:
			#print "DEBUG: COORDINATES (%d,%d) DO NOT EXIST" % (x,y)
			pass
		
		self.get_option()
	#---------------------------------------------


	
	#---------------------------------------------
	def get_option(self):
		print "\t Choose your path wisely. Current coordinates: (%d, %d)" % (self.CurrentX, self.CurrentY)
		tOption = raw_input("> ").lower()
		
		if(tOption == "q" or tOption == "quit"):
			print "Nobody likes a quitter."
			exit(0)
		else:
			tLocation = "%d.%d" % (self.CurrentX, self.CurrentY)
			tIsValid, tX, tY = self.Grid[tLocation].is_door_valid(tOption, self.CurrentX, self.CurrentY)
		
			##print "DEBUG: Valid=%r, tX=%r, tY=%r" % (tIsValid, tX, tY)
		
			if(tIsValid):
				##print "DEBUG: You've chosen a valid option..."
			
				# now we'll figure out what room to enter.
				self.enter_room(tX, tY)
			else:
				print "DEBUG: Invalid option... try reading again."
				print self.create_path_options(self.Grid[tLocation].Doors)
				self.get_option()
	#---------------------------------------------



	#---------------------------------------------
	"""
		Changes "new" to:::
		"There are doors to the [n]orth, [e]ast, and [w]est."
	"""
	def create_path_options(self, pDir):
		tRetval = None
		if(len(pDir) <= 4):
			if(len(pDir) == 4):
				"There is a door in every direction (%s, %s, %s, and %s).  Pick one!" % (
					self.get_direction(pDir[0]), 
					self.get_direction(pDir[1]), 
					self.get_direction(pDir[2]), 
					self.get_direction(pDir[3]))
			elif(len(pDir) == 3):
				tRetval = "There is a door to the %s, %s, and %s." % (
					self.get_direction(pDir[0]), 
					self.get_direction(pDir[1]), 
					self.get_direction(pDir[2]))
			elif(len(pDir) == 2):
				tRetval = "There are two doors, to the %s and %s." % (
					self.get_direction(pDir[0]),
					self.get_direction(pDir[1]))
			else:
				tRetval = "Your only option is to go %s from here." % self.get_direction(pDir)
		else:
			"ERROR: Too many directions defined (%r)" % pDir
			exit(1)

		return(tRetval)
	#---------------------------------------------



	#---------------------------------------------
	def get_direction(self, pDir):
		tRetval = None
		
		if(pDir == "n"):
			tRetval = "[n]orth"
		elif(pDir == "e"):
			tRetval = "[e]ast"
		elif(pDir == "s"):
			tRetval = "[s]outh"
		elif(pDir == "w"):
			tRetval = "[w]est"
		else:
			print "ERROR: invalid direction (%r)" % pDir
			exit(1)
		return (tRetval)
	#---------------------------------------------
		
#==================================
