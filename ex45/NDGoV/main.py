from pprint import pprint
from inspect import getmembers
from globalFunctions import debug_print

"""
 This is my text-based game.... it's based on a 4x4 grid, starting in the 
   upper-left (0,0).  Coordinates are in the form of "X,Y"; all numbers are 
   positive, so in a four-quadrant grid, this would be the bottom-right.
   
The character will have a limited number of hit points, which can be replenished 
using items.  The combat system would be based on a really, really, REALLY 
simplified 3rd edition D&D system... all monsters are pretty much the same, to 
make things simple.

This is my first foray into the world of Python OOP, so... be gentle.


ID's (arbitrary):
	0   == health (restores full HP)
	999 == magic key

"""
from classes import Character, Dungeon, Item, Inventory, DungeonRoom, Monster, Battle

# TODO: Ask the player for their name via raw_input..
#print "What is your character's name?"
#aName = raw_input("> ")


xPlayer = Character("Hard Coded") #aName)
xPlayer.MaxDamage = 10
xDungeon = Dungeon(xPlayer)


xPlayer.Inventory.add_item(0, "Health Potion", 1)
#xPlayer.Inventory.Items[0] = Item(0, "Health Potion", 1)

#### simulate monster attacking player
#xMonster = Monster("Jumping Jahossafat")
#xMonster.MaxDamage = 5
#xMonster.set_hp(30)
#
#xBattle = Battle()
#xBattle.add_player(xPlayer)
#xBattle.add_monster(xMonster)
#xBattle.start()
#
#
#i = 0
#
#exit(0)


xDungeon.add_room( 
	DungeonRoom("s", """
		The dungeon is massive, scary, and poorly documented.  There's a door to 
		the south, it looks to be the only option.  You have no idea why or how 
		you got here.
		""",
		0,0
	)
)
xDungeon.add_room( 
	DungeonRoom("nes", """
		This is, surprisingly, just another room.  
		""",
		1,0
	)
)
xDungeon.add_room( 
	DungeonRoom("ns", """  
		You're getting really far now.
		""",
		2,0
	)
)
xDungeon.add_room( 
	DungeonRoom("ne", """
		Ooh, you reached the southwest corner.  You're brave.
		""",
		3,0
	)
)
xDungeon.add_room( 
	DungeonRoom("esw", """
		A room, just to the east of the starting room.
		This room has a glitch, because you can enter that starting room from 
		it, but you can't get into it from there.  Magic.
		""",
		0,1
	)
)
xDungeon.add_room( 
	DungeonRoom("nsw", """
		IT'S A TRAP!  Before you stands a devious, ascii-based trap monster.
		You cross your eyes for a moment, then unfocusing them to try to discern
		the shape that is before you... your head hurts now.
		""",
		1,1
	)
)
xMonster = Monster("Devious Trap Monster")
xMonster.MaxDamage = 20
xMonster.set_hp(30)
xDungeon.add_monster(xMonster, 1,1)


xDungeon.add_room(
	DungeonRoom("ns", """
		This is a special room, because I forgot to specify a door in the plans.
		So be thankful that I remembered before you got here.
		""",
		2,1
	)
)
xDungeon.add_room( 
	DungeonRoom("new", """
		Southern-most room in the second column of the map.  Very cool room.
		""",
		3,1
	)
)
xDungeon.add_room(
	DungeonRoom("ew", """
		Another room along the north wall of the complex.  It looks square, 
		plain, and... the word "monotonous" comes to mind, but you're not quite 
		sure what that means.  You lost 2 points of intelligence.
		""",
		0,2
	)
)
xDungeon.add_room(
	DungeonRoom("s", """
		YOU FOUND TREASURE!!! There is more gold, silver, and platinum than you 
		could take out in a lifetime.  And when you try, you injure your back.
		You lost 20 hit points (not really).
		""",
		1,2
	)
)
xDungeon.add_room( 
	DungeonRoom("nes", """
		This room is very shiney and made out of all kinds of old consoles. And 
		a lot of Nintendo Entertainment Systems. And that has nothing to do with 
		your available directions of travel.
		""",
		2,2
	)
)
xDungeon.add_room( 
	DungeonRoom("new", """
		This room was just created.  In fact, there's still some scaffolding on 
		the southern wall, blocking any attempt at using the door there, which 
		doesn't exist anyway.
		""",
		3,2
	)
)
xDungeon.add_room(
	DungeonRoom("sw", """
		You found the northeast corner.  You are suddenly filled with pride.
		A moment later, your god punishes you by killing your pet.  That's 
		right, your pet came with you.  And now it's dead.  Aren't you glad you 
		were paying attention?
		""",
		0,3
	)
)
xDungeon.add_room(
	DungeonRoom("ns", """
		There's a plaque in the middle of the room.  It reads:
		
			"Abandon hope, all ye who enter here.
			And don't go to the southwest corner either."
		
		Reading the words makes you happy, you gain 2 points of intelligence.
		""",
		1,3
	)
)
xDungeon.add_room( 
	DungeonRoom("nw", """
		This seems like a corner room to you.  But... is it the corner you want?
		""",
		2,3
	)
)
xDungeon.add_room( 
	DungeonRoom("w", """
		You found the room with the ancient dragon in it. You stop, rub your 
		chin for a momemnt, and realize somebody probably said something to you 
		about find a dragon at the end of a really lame text-based game... er...
		dungeon.  But that was before you took a hit of acid and busted the 
		door down.
		
		There is an epic battle.  It lasts for several minutes, mostly during 
		which you try to figure out how all that green stuff got on your boots 
		and how that dragon fit under your right boot.  You realize the dragon 
		was just a beetle or something, and you're really, really stoned.
		
		CONGRATULATIONS! YOU WIN!
		""",
		3,3
	)
)

## start the game (everything happens in the xDungeon object)
xDungeon.start()
