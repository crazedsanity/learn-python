try:
	from setuptools import setup
except ImportError:
	from distutils.cor import setup
config = {
	'description':		"Console game, written to learn about Python.",
	'author':			"Dan Falconer",
	'url':				"https://bitbucket.org/crazedsanity/learn-python",
	'download_url':		"https://bitbucket.org/crazedsanity/learn-python",
	'author_email':		"dafalconer@nd.gov",
	'version':			"0.1",
	'install_requires':	['nose'],
	'packages':			['NDGoV'],
	'scripts':			[],
	'name':				"ndgov"
}

setup(**config)
