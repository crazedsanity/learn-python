NDGoV - "North Dakota Game of Violence" (working title).

Simple text-based console game.  There's not really much to it, but it's a work in progress, intended for learning Python.


How to install stuff necessary for this to work in Ubuntu:

	sudo apt-get install python-pip python-dev build-essential
	sudo pip install --upgrade pip
	sudo pip install --upgrade virtualenv
	sudo pip install --upgrade distribute
	sudo pip install --upgrade nose
