from pprint import pprint
from inspect import getmembers

"""
 This is my text-based game.... it's based on a 4x4 grid, starting in the 
   upper-left (0,0).  Coordinates are in the form of "X,Y"; all numbers are 
   positive, so in a four-quadrant grid, this would be the bottom-right.
   
The character will have a limited number of hit points, which can be replenished 
using items.  The combat system would be based on a really, really, REALLY 
simplified 3rd edition D&D system... all monsters are pretty much the same, to 
make things simple.

This is my first foray into the world of Python OOP, so... be gentle.
"""

def debug_print(pData):
	pprint(getmembers(pData))
	#pprint(dump(pData))
	
#==================================
class Item(object):
	Id = 0
	Name = "unknown"
	Qty = 0
	
	#---------------------------------------------
	def __init__(self, pId, pName, pQty):
		self.Id = pId
		self.Name = pName
		self.Qty = pQty
	#---------------------------------------------
#==================================



#==================================
class Inventory(object):
	Items = {}
	
	#---------------------------------------------
	def addItem(self, pId, pName, pQty):
		itemObj = Item(pId, pName, pQty)
		try:
			Items[pId] = itemObj
		except:
			# TODO: figure out exceptions
			print "DEBUG: failed to handle adding... ID=(%r) Name=(%r), Qty=(%r)" % (pId, pName, pQty)
			print "DEBUG: %r" % self.Items
			self.replaceItem(pId, pName, pQty)
	#---------------------------------------------
			
	#---------------------------------------------
	def replaceItem(self, pId, pName, pQty):
		itemObj = Item(pId, pName, pQty)
		try:
			del self.Items[pId]
			self.Items[pId] = itemObj
		except:
			print "DEBUG: failed to handle replacement of the object"
			exit(0)
			# TODO: figure out exceptions!
	#---------------------------------------------
#==================================



#==================================
class Character(object):
	Name = "Unknown"
	HP = 100
	MaxHP = 100
	Inventory = Inventory()
	
	
	def __init__(self, name):
		self.Name = name
		self.MaxHP = self.HP
#==================================



#==================================
class DungeonRoom(object):
	
	Doors = ""
	Description = ""
	
	def __init__(self, pDoors, pDescription):
		self.Doors = pDoors.lower()
		self.Description = pDescription
	
	def is_door_valid(self, pDirection, pCurrentX, pCurrentY):
		pDirection = pDirection.lower()
		pDirection = pDirection[0:1]
		
		print "DEBUG: MY DIRECTION: %r, DOORS: %r" % (pDirection, self.Doors)
		
		tIsValid = False
		tX = pCurrentX
		tY = pCurrentY
		if(len(pDirection) > 0 and pDirection in self.Doors):
			tIsValid = True
			
			#now figure out where they're going next
			if(pDirection == 'n'):
				tX = tX - 1
			elif(pDirection == 'e'):
				tY = tY + 1
			elif(pDirection == 's'):
				tX = tX + 1
			elif(pDirection == 'w'):
				tY = tY - 1
			else:
				print "DEBUG: This error should never happen.  And %r is an invalid direction." % pDirection
				exit(1)
		#else:
		#	print "DEBUG: user hit <enter> (%r)" % pDirection
		#	#exit(1)
		#	#self.is_door_valid(pDirection, tX, tY)
		
		print "tX=%r tY=%r" % (tX, tY)
		return (tIsValid, tX, tY)
			
		
#==================================



#==================================
class Dungeon(object):
	
	thePlayer =  Character("Unknown")
	CurrentX = 0
	CurrentY = 0
	Grid = {}
	
	def __init__(self, pPlayer):
		self.thePlayer = pPlayer
	
	def add_room(self, pX, pY, pDungeonRoom):
		theRoom = "%d.%d" % (pX, pY)
		
		if(theRoom in self.Grid):
			print "ERROR::: that room (%r) already exists" % theRoom
			exit(1)
		else:
			self.Grid[theRoom] = pDungeonRoom
	
#	def setup(self):
	
	def start(self):
		print """
		Welcome to my text-based dungeon program!
		"""
		self.enter_room(0,0)
	
	
	def enter_room(self, x,y):
		print "DEBUG: entering room: x=%r y=%r" % (x, y)
		theRoom = "%d.%d" % (x,y)
		print "DEBUG: theRoom=%r" % theRoom
		
		if(theRoom in self.Grid):
			self.CurrentX = x
			self.CurrentY = y
		
			print self.Grid[theRoom].Description
			
		
		else:
			print "DEBUG: COORDINATES (%d,%d) DO NOT EXIST" % (x,y)
		
		self.get_option()
	
	def get_option(self):
		print "\t Choose your path wisely. Current coordinates: (%d, %d)" % (self.CurrentX, self.CurrentY)
		tOption = raw_input("> ").lower()
		
		if(tOption == "q" or tOption == "quit"):
			print "Nobody likes a quitter."
			exit(0)
		else:
			tLocation = "%d.%d" % (self.CurrentX, self.CurrentY)
			tIsValid, tX, tY = self.Grid[tLocation].is_door_valid(tOption, self.CurrentX, self.CurrentY)
		
			#print "DEBUG: Valid=%r, tX=%r, tY=%r" % (tIsValid, tX, tY)
		
			if(tIsValid):
				#print "DEBUG: You've chosen a valid option..."
			
				# now we'll figure out what room to enter.
				self.enter_room(tX, tY)
			else:
				print "DEBUG: Invalid option... try reading again."
				self.get_option()
		
#==================================







#==================================
def show_options(pPlayer, pDoors):
	print "Okay, %s, you have the following options: " % pPlayer.Name
#==================================


#==================================
def show_inventory_options(pPlayer):
	print """
		To use an item in your inventory, just type in 'use X' where the 'X'
		is an item's ID.
	"""
#==================================



"""
ID's (arbitrary):
	0   == health (restores full HP)
	999 == magic key
"""


# TODO: Ask the player for their name via raw_input..
#print "What is your character's name?"
#aName = raw_input("> ")


xPlayer = Character("Hard Coded") #aName)
xDungeon = Dungeon(xPlayer)


#xPlayer.Inventory.addItem(0, "Health Potion", 1)
xPlayer.Inventory.Items[0] = Item(0, "Health Potion", 1)

xDungeon.add_room(0, 0, 
	DungeonRoom("s", """
		The dungeon is massive, scary, and poorly documented.  There's a door to 
		the south, it looks to be the only option.  You have no idea why or how 
		you got here.
		
		Your only option is to go [s]outh from here.
		"""
	)
)
xDungeon.add_room(1, 0, 
	DungeonRoom("nes", """
		This is, surprisingly, just another room.  
		
		You can go [n]orth, [e]ast, or [s]outh.
		"""
	)
)
xDungeon.add_room(2, 0, 
	DungeonRoom("ns", """  
		You're getting really far now.
		
		There's two doors in this room, to the [n]orth and to the [s]outh.
		"""
	)
)
xDungeon.add_room(3, 0, 
	DungeonRoom("ne", """
		Ooh, you reached the southwest corner.  You're brave.
		
		There's a door to the [n]orth and to the [e]ast.
		"""
	)
)
xDungeon.add_room(0, 1, 
	DungeonRoom("esw", """
		A room, just to the east of the starting room.
		This room has a glitch, because you can enter that starting room from 
		it, but you can't get into it from there.  Magic.
		
		There's a door to the [e]ast, [s]outh, and [w]est.
		"""
	)
)
xDungeon.add_room(1, 1, 
	DungeonRoom("nsw", """
		IT'S A TRAP!  You should be getting tested to see if you can escape this 
		devious, ascii-based trap.  But this dungeon isn't good enough yet.
		
		There are doors to the [n]orth, [s]outh, and [w]est.
		"""
	)
)
xDungeon.add_room(2, 1,
	DungeonRoom("ns", """
		This is a special room, because I forgot to specify a door in the plans.
		So be thankful that I remembered before you got here.
		
		There are doors to the [n]orth and [s]outh.
		"""
	)
)
xDungeon.add_room(3, 1, 
	DungeonRoom("new", """
		Southern-most room in the second column of the map.  Very cool room.
		
		There are doors to the [n]orth, [e]ast, and [w]est.
		"""
	)
)
xDungeon.add_room(0, 2, 
	DungeonRoom("ew", """
		Another room along the north wall of the complex.  It looks square, 
		plain, and... the word "monotonous" comes to mind, but you're not quite 
		sure what that means.  You lost 2 points of intelligence.
		
		There are two doors, to the [e]ast and the [w]est.
		"""
	)
)
xDungeon.add_room(1, 2, 
	DungeonRoom("s", """
		YOU FOUND TREASURE!!! There is more gold, silver, and platinum than you 
		could take out in a lifetime.  And when you try, you injure your back.
		You lost 20 hit points (not really).
		
		Your only option is to head [s]outh.
		"""
	)
)
xDungeon.add_room(2, 2, 
	DungeonRoom("nes", """
		This room is very shiney and made out of all kinds of old consoles. And 
		a lot of Nintendo Entertainment Systems. And that has nothing to do with 
		your available directions of travel.
		
		You can go [n]orth, [e]ast, or [s]outh.
		"""
	)
)
xDungeon.add_room(3, 2, 
	DungeonRoom("new", """
		This room was just created.  In fact, there's still some scaffolding on 
		the southern wall, blocking any attempt at using the door there, which 
		doesn't exist anyway.
		
		You can go [n]orth, [e]ast, or [w]est.
		"""
	)
)
xDungeon.add_room(0, 3, 
	DungeonRoom("sw", """
		You found the northeast corner.  You are suddenly filled with pride.
		A moment later, your god punishes you by killing your pet.  That's 
		right, your pet came with you.  And now it's dead.  Aren't you glad you 
		were paying attention?
		
		You can go [s]outh or [w]est.
		"""
	)
)
xDungeon.add_room(1, 3, 
	DungeonRoom("ns", """
		There's a plaque in the middle of the room.  It reads:
		
			"Abandon hope, all ye who enter here.
			And don't go to the southwest corner either."
		
		Reading the words makes you happy, you gain 2 points of intelligence.
		
		You can go [n]orth or [s]outh.
		"""
	)
)
xDungeon.add_room(2, 3, 
	DungeonRoom("nw", """
		This seems like a corner room to you.  But... is it the corner you want?
		
		You can go [n]orth or [w]est.
		"""
	)
)
xDungeon.add_room(3, 3, 
	DungeonRoom("w", """
		You found the room with the ancient dragon in it. You stop, rub your 
		chin for a momemnt, and realize somebody probably said something to you 
		about find a dragon at the end of a really lame text-based game... er...
		dungeon.  But that was before you took a hit of acid and busted the 
		door down.
		
		There is an epic battle.  It lasts for several minutes, mostly during 
		which you try to figure out how all that green stuff got on your boots 
		and how that dragon fit under your right boot.  You realize the dragon 
		was just a beetle or something, and you're really, really stoned.
		
		CONGRATULATIONS! YOU WIN!
		
		Unfortunately, due to poor programming, nothing special happens.  And 
		you have to either type 'q' or press <CTRL> + c to end.
		
		But hey, at least you're not a quitter.
		"""
	)
)

xDungeon.start()



